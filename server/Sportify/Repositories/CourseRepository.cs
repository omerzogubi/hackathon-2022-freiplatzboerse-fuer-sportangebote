﻿using Sportify.AppDbContext;
using Sportify.Models;
using Sportify.Repositories.Interfaces;
using Sportify.Utility;

namespace Sportify.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        private readonly SportifyDbContext _dbContext;

        public CourseRepository()
        {
            _dbContext = new SportifyDbContext();
        }
        public List<Course> GetAll()
        {
            var list = MockData.Courses.ToList();
            return list;
        }

        public List<Course> GetCourse(string provider)
        {
            var list = MockData.Courses.Where(x => x.ProviderId == provider).ToList();
            return list;
        }
    }
}
