﻿using Microsoft.EntityFrameworkCore;
using Sportify.AppDbContext;
using Sportify.Models;
using Sportify.Repositories.Interfaces;
using Sportify.Utility;

namespace Sportify.Repositories
{
    public class ProviderRepository : IProviderRepository
    {
        private readonly SportifyDbContext _dbContext;

        public ProviderRepository()
        {
            _dbContext = new SportifyDbContext();
        }

        public  List<Provider> GetAll()
        {
            var list = MockData.Providers.ToList();
            return list;
        }

        public void Save(Event courseEvent)
        {
            MockData.Events.Add(courseEvent);
        }
    }
}
