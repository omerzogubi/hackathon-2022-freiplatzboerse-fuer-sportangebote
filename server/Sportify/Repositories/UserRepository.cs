﻿using Microsoft.EntityFrameworkCore;
using Sportify.AppDbContext;
using Sportify.Models;
using Sportify.Repositories.Interfaces;
using Sportify.Utility;

namespace Sportify.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly SportifyDbContext _dbContext ;

        public UserRepository()
        {
            _dbContext =  new SportifyDbContext();
        }

        public User GetUser(string email, string password)
        {
            return MockData.Users.Where(x => x.Email == email && x.Password == password).FirstOrDefault();
        }

        public  List<User> GetUsers()
        {
            var list = MockData.Users.ToList();
            return list;
        }

        public void Save(User user)
        {
            MockData.Users.Add(user);
        }
    }
}
