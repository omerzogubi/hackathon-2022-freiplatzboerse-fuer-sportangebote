﻿using Microsoft.EntityFrameworkCore;
using Sportify.AppDbContext;
using Sportify.Models;
using Sportify.Repositories.Interfaces;
using Sportify.Utility;

namespace Sportify.Repositories
{
    public class EventRepository : IEventRepository
    {
        private readonly SportifyDbContext _dbContext;

        public EventRepository()
        {
            _dbContext = new SportifyDbContext();
        }

        public  List<Event> GetAll()
        {

            var list = MockData.Events.ToList();
            return list;
        }

        public void Save(Event courseEvent)
        {
            MockData.Events.Add(courseEvent);
        }
    }
}
