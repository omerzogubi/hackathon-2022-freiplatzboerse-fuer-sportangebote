﻿using Sportify.Models;

namespace Sportify.Repositories.Interfaces
{
    public interface ICourseRepository
    {
        public List<Course> GetAll();
        List<Course> GetCourse(string provider);
    }
}
