﻿using Sportify.Models;

namespace Sportify.Repositories.Interfaces
{
    public interface IUserRepository
    {
        public List<User> GetUsers();
        void Save(User user);
        User GetUser(string email, string password);
    }
}
