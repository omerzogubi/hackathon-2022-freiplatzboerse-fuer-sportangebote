﻿using Sportify.Models;

namespace Sportify.Repositories.Interfaces
{
    public interface IProviderRepository
    {
        public List<Provider> GetAll();
    }
}
