﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sportify.Models;
using Sportify.Repositories.Interfaces;
using Sportify.Services.Interfaces;

namespace Sportify.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public ActionResult<List<User>> Get()
        {
            return Ok(_userService.GetAll());
        }

        [HttpGet("{email}/{password}")]
        public ActionResult<User>Login(string email, string password)
        {
            User user = _userService.GetUser(email, password);
            if (user == null)
            {
                return BadRequest("Credential is incorrect");
            }
                
            return Ok();
        }

        [HttpPost]
        public ActionResult<List<User>> Register(User user)
        {
            _userService.Save(user);
            return Ok();
        }

        [HttpDelete]
        public ActionResult<List<Provider>> Delete(User user)
        {
            //will be implemented in the next phase
            return Ok(user);
        }
    }
}
