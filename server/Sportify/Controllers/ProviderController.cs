﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sportify.Models;
using Sportify.Repositories.Interfaces;

namespace Sportify.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProviderController : ControllerBase
    {
        private readonly IProviderRepository _providerRepository;

        public ProviderController(IProviderRepository providerRepository)
        {
            _providerRepository = providerRepository;
        }

        [HttpGet]
        public ActionResult<List<Provider>> Get()
        {
            return Ok(_providerRepository.GetAll());
        }

        [HttpGet("{provider}")]
        public ActionResult<List<Provider>> Get(string provider)
        {
            //will be implemented in the next phase
            return Ok(provider);
        }

        [HttpPost]
        public ActionResult<List<Provider>> Add(Provider provider)
        {
            //will be implemented in the next phase
            return Ok(provider);
        }

        [HttpDelete]
        public ActionResult<List<Provider>> Delete(Provider provider)
        {
            //will be implemented in the next phase
            return Ok(provider);
        }

    }
}
