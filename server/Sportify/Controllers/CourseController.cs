﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sportify.AppDbContext;
using Sportify.Models;
using Sportify.Repositories.Interfaces;

namespace Sportify.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ICourseRepository _courseRepository;

        public CourseController(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        [HttpGet]
        public ActionResult<List<Course>> Get()
        {
            return Ok(_courseRepository.GetAll());
        }

        [HttpGet("{provider}")]
        public ActionResult<Course> Get(string provider)
        {
            return Ok(_courseRepository.GetCourse(provider));
        }

    }
}
