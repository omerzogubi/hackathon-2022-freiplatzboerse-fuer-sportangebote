﻿using Microsoft.AspNetCore.Mvc;
using Sportify.Models;
using Sportify.Services.Interfaces;

namespace Sportify.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly IEventService _eventService;

        public EventController(IEventService eventService)
        {
            _eventService = eventService;
        }
        [HttpGet]
        public ActionResult<List<Event>> Get()
        {
            return Ok(_eventService.GetAll());
        }

        [HttpPost]
        public ActionResult<List<User>> Add(Event courseEvent)
        {
            _eventService.Save(courseEvent);
            return Ok();
        }

        [HttpPut]
        public ActionResult<List<User>> Update(Event courseEvent)
        {
            //Will be implemented in next phase
            return Ok(courseEvent);
        }

        [HttpDelete]
        public ActionResult<List<User>> Delete(Event courseEvent)
        {
            //Will be implemented in next phase
            return Ok(courseEvent);
        }

    }
}
