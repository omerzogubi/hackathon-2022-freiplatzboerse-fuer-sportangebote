﻿namespace Sportify.Models
{
    public class Course
    {
        public Guid Id { get; set; }
        public string ProviderId { get; set; }
        public string Name { get; set; }
        public string Subtitle { get; set; }
        public string Level { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool BarrierFree { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public decimal Price { get; set; }
        public string LectureId { get; set; }
        public string WebURL { get; set; }
        public string Language { get; set; }
        public string CategoryId { get; set; }
        public string RequiredSkill { get; set; }
    }
}
