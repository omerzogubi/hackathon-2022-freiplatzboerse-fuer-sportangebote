﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Sportify.Models
{
   
    public class Event
    {
        [Key]
        public Guid Id { get; set; }
        public string CreatorId { get; set; }
        public string ExchangeId { get; set; }
        public int Price { get; set; }
        public List<DateTime>? Dates { get; set; }
        public string? Role { get; set; }
        public string? Description { get; set; }
        public string? Status { get; set; }
        public int ReportCounter { get; set; }
        public string CourseId { get; set; }
        public string CategoryId { get; set; }
        public string RequiredSkill { get; set; }
    }
}
