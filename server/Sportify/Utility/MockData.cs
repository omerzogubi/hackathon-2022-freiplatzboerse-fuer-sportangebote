﻿using Sportify.Models;

namespace Sportify.Utility
{
    public static class MockData
    {
        public static List<User> Users { get; set; } = new List<User>()
        {
            new User()
            {
                Id = new Guid("CF67BFB1-4721-42F0-8251-CA14C540659E"),
                FirstName = "Rakib",
                LastName = "Hasan",
                Email = "Test@gmail.com",
                Password = "123300",
                Role = "Admin",
                PhoneNumber = "015483021564"
            },
        };
        public static List<Event> Events { get; set; } = new List<Event>()
        {
            new Event()
            {
                Id = new Guid("1C65C4B1-02ED-4AF2-AE4F-11E623838A09"),
                CreatorId = "CF67BFB1-4721-42F0-8251-CA14C540659E",
                Price = 2,
                Dates = new List<DateTime> { DateTime.Now, DateTime.Today },
                Status = "Pending",
                Role = "Admin",
                Description = "test",
                ReportCounter = 2,
                CourseId = "574E5C84-ABA2-4251-BB58-C782A7F47068",
                CategoryId = "CF67BFB1-4721-42F0-8251-CA14C540659E",
                ExchangeId = "CF67BFB1-4721-42F0-8251-CA14C540659E",
                RequiredSkill = "Basic Swimming"
            },
        };
        public static List<Course> Courses { get; set; } = new List<Course>()
        {
            new Course()
            {
                Id = new Guid("574E5C84-ABA2-4251-BB58-C782A7F47068"),
                ProviderId = "9157E217-1951-4E84-8B9B-78C880144BC2",
                Name = "Test Name",
                Subtitle = "Test",
                Level = "Beginer",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                BarrierFree = true,
                Price = 2,
                StartTime = DateTime.Now,
                EndTime = DateTime.Now,
                LectureId = "CF67BFB1-4721-42F0-8251-CA14C540659E",
                WebURL = "Google.com",
                Language = "En",
                CategoryId = "CF67BFB1-4721-42F0-8251-CA14C540659E"
               
            },
        };
        public static List<Provider> Providers { get; set; } = new List<Provider>()
        {
            new Provider()
            {
                Id = new Guid("9157E217-1951-4E84-8B9B-78C880144BC2"),
                Name = "VHS -Fulda",
                RegulationURL = "vhs.com"
            },
        };
    }
}
