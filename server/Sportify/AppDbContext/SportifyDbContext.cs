﻿using Microsoft.EntityFrameworkCore;
using Sportify.Models;

namespace Sportify.AppDbContext
{
    public class SportifyDbContext : DbContext
    {
        protected override void OnConfiguring
        (DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "SportifyDb");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = new Guid("CF67BFB1-4721-42F0-8251-CA14C540659E"),
                    FirstName = "Rakib",
                    LastName = "Hasan",
                    Email = "Test@gmail.com",
                    Password = "123300",
                    PhoneNumber = "015483021564"
                });

            modelBuilder.Entity<Event>()
                .HasData(new Event
                {
                    Id = new Guid("1C65C4B1-02ED-4AF2-AE4F-11E623838A09"),
                    CreatorId = "CF67BFB1-4721-42F0-8251-CA14C540659E",
                    Price = 2,
                    //Dates = new List<DateTime> { DateTime.Now, DateTime.Today },
                    Status = "Pending",
                    Role = "Admin",
                    Description = "test",
                    ReportCounter = 2,
                    CourseId = "CF67BFB1-4721-42F0-8251-CA14C540659E",
                    CategoryId = "CF67BFB1-4721-42F0-8251-CA14C540659E",
                    ExchangeId = "CF67BFB1-4721-42F0-8251-CA14C540659E",
                });
        }

        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Event> Events { get; set; } = null!;
        public DbSet<Course> Courses { get; set; } = null!;
        public DbSet<Provider> Providers { get; set; } = null!;
    }
}
