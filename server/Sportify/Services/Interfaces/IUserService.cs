﻿using Sportify.Models;

namespace Sportify.Services.Interfaces
{
    public interface IUserService
    {
        object? GetAll();
        void Save(User user);
        User GetUser(string email, string password);
    }
}
