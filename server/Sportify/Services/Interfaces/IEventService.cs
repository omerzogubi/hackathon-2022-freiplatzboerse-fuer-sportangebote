﻿using Sportify.Models;

namespace Sportify.Services.Interfaces
{
    public interface IEventService
    {
        object? GetAll();
        void Save(Event courseEvent);
    }
}
