﻿using Sportify.Models;
using Sportify.Repositories.Interfaces;
using Sportify.Services.Interfaces;

namespace Sportify.Services
{
    public class EventService : IEventService
    {
        private readonly IEventRepository _eventRepository;
        public EventService(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public object? GetAll()
        {
            return _eventRepository.GetAll();
        }

        public void Save(Event courseEvent)
        {
            _eventRepository.Save(courseEvent);
        }
    }
}
