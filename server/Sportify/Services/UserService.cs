﻿using Sportify.Models;
using Sportify.Repositories.Interfaces;
using Sportify.Services.Interfaces;

namespace Sportify.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public object? GetAll()
        {
            return _userRepository.GetUsers();
        }

        public User GetUser(string email, string password)
        {
            return _userRepository.GetUser(email, password);
        }

        public void Save(User user)
        {
            _userRepository.Save(user);
        }
    }
}
