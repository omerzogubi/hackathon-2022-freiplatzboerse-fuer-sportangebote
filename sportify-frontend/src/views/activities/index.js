export { default as ConfirmActivityView } from './ConfirmActivityView.vue';
export { default as CreateActivityView } from './CreateActivityView.vue';
export { default as EditActivityView } from './EditActivityView.vue';
export { default as ViewActivityView } from './ViewActivityView.vue';