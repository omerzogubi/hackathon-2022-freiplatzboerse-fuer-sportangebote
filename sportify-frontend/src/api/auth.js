const _users = [
    { id: 0, email: 'kevin.leirich@gmail.com', password: 'password1234', firstName: 'Kevin', lastName: 'Leirich' }
]
let _id = 1

export default {
    async login({ email, password }) {
        await wait(100)

        return _users.find(user => user.email === email && user.password === password) ?? null
    },
    async register(user) {
        await wait(100)
        _users.push({ _id, ...user })
        let id = _id;
        _id++;
        return _users.find(user => user.id === id);
    },
    async logout(user) {
        await wait(100)
    }
}

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}