const _categories = [
    { id: 0, name: '' }
]

const _providers = [
    { id: 0, name: 'Hochschule Fulda' },
    { id: 1, name: 'Volkshochschule Fulda' }
]

const _activities = [
    { id: 0, name: 'Akrobatik', description: 'Akrobatik Kurs', dates: [], provider: 0 },
    { id: 1, name: 'Badminton', description: 'Badminton Kurs', dates: [], provider: 0 },
    { id: 2, name: 'Schwimmen', description: 'Schwimmkurs Seepferdchen', dates: [], provider: 1 },
    { id: 3, name: 'Klettern', description: 'Klettern im DAV-Kletterzentrum', dates: [], provider: 1 },
]

export default {
    async getProviders() {
        await wait(100)
        return _providers
    },
    async getActivities() {
        await wait(100)
        return _activities
    },
    async postActivity(activity) {
        await wait(100)
        _activities.push(activity)
    }
}

function wait(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms)
    })
}