import auth from "../../api/auth";

const state = () => ({
    loggedIn: false,
    user: null
})

const getters = {
    loggedIn: (state, getters) => {
        return state.loggedIn;
    },
    user: (state, getters) => {
        return state.user;
    },
    roles: (state, getters) => {
        return state.user?.roles;
    }
}

const actions = {
    async register( { commit }, userData) {
        const user = await auth.register(userData)
        commit('registerSuccess', user)
    },
    async login({ commit }, login) {
        const user = await auth.login(login)

        if (user) {
            commit('loginSuccess', user)
        } else {
            commit('loginFailure')
        }    
    },
    async logout({ commit }) {
        await auth.logout
        commit('logout');
    }
}

const mutations = {
    loginSuccess(state, user) {
        state.loggedIn = true;
        state.user = user;
    },
    loginFailure(state) {
        state.loggedIn = false;
    },
    logout(state) {
        state.loggedIn = false;
        state.user = null;
    },
    registerSuccess(state) {
    },
    registerFailure(state) {        
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}