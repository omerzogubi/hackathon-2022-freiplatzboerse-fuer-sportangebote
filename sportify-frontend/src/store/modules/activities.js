import activityAPI from "../../api/activities"

const state = () => ({
    providers: [],
    activities: []
})

const getters = {
    activities(state) {
        return state.activities
    },
    activityById(state, id) {
        return state.activities
            .search(activity => activity.id === id)
    },
    providers() {
        return state.providers
    }
}

const actions = {
    async fetchActivities( { commit } ) {
        const activities = await activityAPI.getActivities();
        commit('activitiesFetched', activities)
    },
    async fetchProviders( { commit } ) {
        const providers = await activityAPI.getProviders();
        commit('providersFetched', providers)
    },
    async postActivity( { commit }, newActivity) {
        const activity = await activityAPI.postActivity(newActivity);
        commit('postActivity', activity)
    }
}

const mutations = {
    activitiesFetched(state, activities) {
        state.activities = activities
    },
    providersFetched(state, providers) {
        state.providers = providers
    },
    postActivity(state, activity) {
        state.activities.push(activity)
    }
}

export default {
    namespaced: true,
    state, 
    getters,
    actions,
    mutations
}